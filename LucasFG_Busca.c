//Author
//LUCAS FRISON GONCALVES
//GRR20204461

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct t_time {
    int hours;
    int minutes;
    int seconds;
} t_time;

int time_cmp(t_time hour, t_time hour2);

typedef struct t_timetable {
    t_time* keys;
    char** values;
    int size;
} t_timetable;

void put(t_time key, char * val);
char * get(t_time key);
void delete(t_time key);
int contains(t_time key);
int is_empty();
int size();
t_time min();
t_time max();
t_time t_floor(t_time key);
t_time ceiling(t_time key);
int rank(t_time key);
t_time select(int k);
void delete_min();
void delete_max();
int size_range(t_time lo, t_time hi);
t_time * keys(t_time lo, t_time hi);
int buscaBinaria(t_time key, int inicio, int fim);
void quickSort(int first, int last);

t_timetable timeTable;

int time_cmp(t_time hour, t_time hour2) {

    int hora1 = hour.hours * 3600 + hour.minutes * 60 + hour.seconds;
    int hora2 = hour2.hours * 3600 + hour2.minutes * 60 + hour2.seconds; 

    if (hora1 > hora2)
        return 1;
    if (hora1 < hora2)
        return -1;
    
    return 0;        
}

void put(t_time key, char * val) {
    if (contains(key) == 0)
        delete(key);   
    timeTable.keys[size()] = key;
    timeTable.values[size()] = val;
    timeTable.size++;
    quickSort(0, size());
}

char * get(t_time key) {
    int result = buscaBinaria(key, 0, size());
    if (result != -1 && timeTable.values[result] != "")  
        return timeTable.values[result];
    return NULL;
}

void delete(t_time key) {
    int pos = buscaBinaria(key, 0, size());
    timeTable.values[pos] = "";
}

int contains(t_time key) {
    char * result = get(key);
    return (result != NULL && result != "" ? 1 : 0);
}

int is_empty() {
    if (size() == 0)
        return 1;
    return 0;    
}

int size() {
    return timeTable.size;
}

t_time min() {
    return timeTable.keys[0];
}

t_time max() {
    return timeTable.keys[size() - 1];
}

t_time t_floor(t_time key) {
    int floorIndex = buscaBinaria(key, 0, size()) - 1;
    return timeTable.keys[floorIndex];
}

t_time ceiling(t_time key) {
    int ceilingIndex = buscaBinaria(key, 0, size()) + 1;
    return timeTable.keys[ceilingIndex];
}

int rank(t_time key) {
    return size_range(timeTable.keys[0], key) + 1;
}

t_time select(int k) {
    return timeTable.keys[rank(timeTable.keys[k])];
}

void delete_min() {
    delete(timeTable.keys[0]);
}

void delete_max() {
    delete(timeTable.keys[size() - 1]);
}

int size_range(t_time lo, t_time hi) {
    int lowIndex = buscaBinaria(lo, 0, size());
    int highIndex = buscaBinaria(hi, lowIndex, size());
    return highIndex - lowIndex - 1;
}

t_time * keys(t_time lo, t_time hi) {
    t_time * t_time = malloc(sizeof(t_time) * size_range(lo, hi));
    int lowIndex = buscaBinaria(lo, 0, size());
    int highIndex = buscaBinaria(hi, lowIndex, size());
    for (int i = lowIndex; i <= highIndex; i++) {
        t_time[i] = timeTable.keys[i];   
    }
    return t_time;
}

int buscaBinaria(t_time key, int inicio, int fim) {
    int meio = 0;
    while (inicio <= fim) {
        if (time_cmp(key, timeTable.keys[meio]) == 0) {
            return meio; 
        } else if (time_cmp(key, timeTable.keys[meio]) == 1) {
            inicio = meio + 1;
        } else {
            fim = meio - 1;
        }  
        meio = (inicio + fim) / 2;
    }   
    return -1;          
}

void quickSort(int first, int last) {
    int i, j, pivot;
    t_time temp;
    char * tempVal;
    
    if(first < last){
       
        pivot = first;
        i = first;
        j = last - 1;
        
        while(i < j){
            while(time_cmp(timeTable.keys[i], timeTable.keys[pivot]) == -1 && i < last) i++;
            while(time_cmp(timeTable.keys[j], timeTable.keys[pivot]) == 1) j--;
            if(i < j) {
                temp = timeTable.keys[i];
                tempVal = timeTable.values[i];
                timeTable.keys[i] = timeTable.keys[j];
                timeTable.keys[j] = temp;
                timeTable.values[i] = timeTable.values[j];
                timeTable.values[j] = tempVal;
            }
        }
       
        temp = timeTable.keys[pivot];
        tempVal = timeTable.values[i];
        timeTable.keys[pivot] = timeTable.keys[j];
        timeTable.values[pivot] = timeTable.values[j];
        timeTable.keys[j] = temp;
        timeTable.values[j] = tempVal;
        quickSort(first, j - 1);
        quickSort(j + 1, last);
   }
}

int main(int argc, char const *argv[])
{
    timeTable.keys = malloc(sizeof(t_time) * 10);
    timeTable.values = malloc(sizeof(char*) * 10);
    timeTable.size = 0;

    t_time time;
    time.hours = 1;
    time.minutes = 1;
    time.seconds = 1;
    put(time, "Houston");

    time.hours = 6;
    time.minutes = 40;
    time.seconds = 20;
    put(time, "Curitiba");

    time.hours = 20;
    time.minutes = 1;
    time.seconds = 59;
    put(time, "Chicago");
    printf("Rank Chicago: %d\n", rank(time));
    printf("Max: %d:%d:%d\t", max().hours, max().minutes, max().seconds);
    printf("Min: %d:%d:%d\n", min().hours, min().minutes, min().seconds);
    
    printf("\nTabela completa:\n");
    for (int i = 0; i < size(); i++) {
        if (contains(timeTable.keys[i])) {
            printf("%d:%d:%d\t", timeTable.keys[i].hours, timeTable.keys[i].minutes, timeTable.keys[i].seconds);
            printf("%s\n", get(timeTable.keys[i]));
        }
    }

    printf("\nTabela apos delete:\n");
    delete(time);
    for (int i = 0; i < size(); i++) {
        if (contains(timeTable.keys[i])) {
            printf("%d:%d:%d\t", timeTable.keys[i].hours, timeTable.keys[i].minutes, timeTable.keys[i].seconds);
            printf("%s\n", get(timeTable.keys[i]));
        }
    }

    printf("\nTabela do metodo keys():\n");
    t_time * tabela = keys(timeTable.keys[0], timeTable.keys[size()-1]);
    for (int i = 0; i < size(); i++) {
        if (contains(tabela[i])) {
            printf("%d:%d:%d\t", tabela[i].hours, tabela[i].minutes, tabela[i].seconds);
        }
    }

    t_time t = select(1);
    printf("\nTeste do metodo select():\n");
    printf("%d:%d:%d\t", t.hours, t.minutes, t.seconds);

    t_time t2 = t_floor(t);
    printf("\nTeste do metodo t_floor():\n");
    printf("%d:%d:%d\t", t2.hours, t2.minutes, t2.seconds);

    return 0;
}
